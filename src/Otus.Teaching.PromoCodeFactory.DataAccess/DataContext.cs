﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess;

public class DataContext : DbContext
{
    private const int MaxLength = 100;
    
    public DataContext()
    { }
    
    public DataContext(DbContextOptions<DataContext> options) 
        : base(options)
    { }
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Employee>().HasKey(x => x.Id);
        modelBuilder.Entity<Employee>().Property(x => x.FirstName).HasMaxLength(MaxLength);
        modelBuilder.Entity<Employee>().HasIndex(x => x.FirstName);
        modelBuilder.Entity<Employee>().Property(x => x.LastName).HasMaxLength(MaxLength);
        modelBuilder.Entity<Employee>().HasIndex(x => x.LastName);
        modelBuilder.Entity<Employee>().Property(x => x.Email).HasMaxLength(MaxLength);
        modelBuilder.Entity<Employee>().HasIndex(x => x.Email).IsUnique();
        modelBuilder.Entity<Employee>().HasOne(x => x.Role).WithMany().HasForeignKey(x => x.RoleId);

        modelBuilder.Entity<Role>().HasKey(x => x.Id);
        modelBuilder.Entity<Role>().Property(x => x.Name).HasMaxLength(MaxLength);
        modelBuilder.Entity<Role>().HasIndex(x => x.Name).IsUnique();
        modelBuilder.Entity<Role>().Property(x => x.Description).HasMaxLength(MaxLength);

        modelBuilder.Entity<Preference>().HasKey(x => x.Id);
        modelBuilder.Entity<Preference>().Property(x => x.Name).HasMaxLength(MaxLength);

        modelBuilder.Entity<Customer>().HasKey(x => x.Id);
        modelBuilder.Entity<Customer>().Property(x => x.Firstname).HasMaxLength(MaxLength);
        modelBuilder.Entity<Customer>().HasIndex(x => x.Firstname);
        modelBuilder.Entity<Customer>().Property(x => x.Surname).HasMaxLength(MaxLength);
        modelBuilder.Entity<Customer>().HasIndex(x => x.Surname);
        modelBuilder.Entity<Customer>().Property(x => x.Email).HasMaxLength(MaxLength);
        modelBuilder.Entity<Customer>().HasIndex(x => x.Email).IsUnique();
        modelBuilder.Entity<Customer>().HasMany(x => x.PromoCodes).WithOne().IsRequired();
        modelBuilder.Entity<Customer>().HasMany(x => x.Preferences).WithOne().HasForeignKey(x => x.CustomerId).IsRequired();
        
        modelBuilder.Entity<PromoCode>().HasKey(x => x.Id);
        modelBuilder.Entity<PromoCode>().Property(x => x.Code).HasMaxLength(MaxLength);
        modelBuilder.Entity<PromoCode>().HasIndex(x => x.Code).IsUnique();
        modelBuilder.Entity<PromoCode>().Property(x => x.ServiceInfo).HasMaxLength(MaxLength);
        modelBuilder.Entity<PromoCode>().Property(x => x.PartnerName).HasMaxLength(MaxLength);
        modelBuilder.Entity<PromoCode>().HasOne(x => x.PartnerManager).WithMany().HasForeignKey(x => x.PartnerManagerId);
        modelBuilder.Entity<PromoCode>().HasOne(x => x.Preference).WithMany().HasForeignKey(x => x.PreferenceId);
        
        modelBuilder.Entity<Role>().HasData(FakeDataFactory.Roles);
    }

    public DbSet<Employee> Employees { get; set; }
    public DbSet<Role> Roles { get; set; }
    public DbSet<Customer> Customers { get; set; }
    public DbSet<Preference> Preferences { get; set; }
    public DbSet<PromoCode> PromoCodes  { get; set; }
    public DbSet<CustomerPreference> CustomerPreferences { get; set; }
}