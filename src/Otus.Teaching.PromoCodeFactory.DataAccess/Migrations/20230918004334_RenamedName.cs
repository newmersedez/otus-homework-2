﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations
{
    public partial class RenamedName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "FirstName",
                table: "Customers",
                newName: "Firstname");

            migrationBuilder.RenameColumn(
                name: "LastName",
                table: "Customers",
                newName: "Surname");

            migrationBuilder.RenameIndex(
                name: "IX_Customers_FirstName",
                table: "Customers",
                newName: "IX_Customers_Firstname");

            migrationBuilder.RenameIndex(
                name: "IX_Customers_LastName",
                table: "Customers",
                newName: "IX_Customers_Surname");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Firstname",
                table: "Customers",
                newName: "FirstName");

            migrationBuilder.RenameColumn(
                name: "Surname",
                table: "Customers",
                newName: "LastName");

            migrationBuilder.RenameIndex(
                name: "IX_Customers_Firstname",
                table: "Customers",
                newName: "IX_Customers_FirstName");

            migrationBuilder.RenameIndex(
                name: "IX_Customers_Surname",
                table: "Customers",
                newName: "IX_Customers_LastName");
        }
    }
}
