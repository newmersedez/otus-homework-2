﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersController(
            IRepository<Customer> customerRepository, 
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получение списка клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();
            var result = customers
                .Select(x => new CustomerShortResponse
                {
                    Id = x.Id,
                    FirstName = x.Firstname,
                    LastName = x.Surname,
                    Email = x.Email
                })
                .ToList();

            return Ok(result);
        }
        
        /// <summary>
        /// Получение информации о клиенте
        /// </summary>
        /// <param name="id">Идентификатор клиента</param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer is null) return NotFound("Клиент не найден");

            var result = new CustomerResponse
            {
                Id = customer.Id,
                FirstName = customer.Firstname,
                LastName = customer.Surname,
                Email = customer.Email,
                Preferences = customer.Preferences
                    .Select(x => new CustomerPreferenceResponse
                    {
                        Id = x.PreferenceId,
                        Name = x.Preference.Name
                    })
                    .ToList(),
                PromoCodes = customer.PromoCodes.Select(x => new PromoCodeShortResponse
                    {
                        Id = x.Id,
                        Code = x.Code,
                        ServiceInfo = x.ServiceInfo,
                        BeginDate = x.BeginDate.ToString(CultureInfo.InvariantCulture),
                        EndDate = x.EndDate.ToString(CultureInfo.InvariantCulture),
                        PartnerName = x.PartnerName
                    })
                    .ToList()
            };

            return Ok(result);
        }
        
        /// <summary>
        /// Создание клиента
        /// </summary>
        /// <param name="request">Тело запроса</param>
        /// <returns>Идентификатор нового клиента</returns>
        [HttpPost]
        public async Task<ActionResult<Guid>> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var preferences = (await _preferenceRepository.GetAllAsync(request.PreferenceIds)).ToList();

            var customer = new Customer
            {
                Id = Guid.NewGuid(),
                Firstname = request.FirstName,
                Surname = request.LastName,
                Email = request.Email
            };

            customer.Preferences = preferences
                .Select(x => new CustomerPreference { PreferenceId = x.Id })
                .ToList();

            _customerRepository.Add(customer);
            await _customerRepository.SaveAsync();
            
            return Ok(customer.Id);
        }
        
        /// <summary>
        /// Редактирование клиента
        /// </summary>
        /// <param name="id">Идентификатор клиента</param>
        /// <param name="request">Тело запроса</param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<ActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer is null) return NotFound("Клиент не найден");
            
            var preferences = (await _preferenceRepository.GetAllAsync(request.PreferenceIds)).ToList();

            customer.Firstname = request.FirstName;
            customer.Surname = request.LastName;
            customer.Email = request.Email;

            customer.Preferences = preferences
                .Select(x => new CustomerPreference { PreferenceId = x.Id })
                .ToList();

            await _customerRepository.SaveAsync();
            
            return Ok();
        }
        
        /// <summary>
        /// Удаление клиента
        /// </summary>
        /// <param name="id">Идентификатор клиента</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer is null) return NotFound("Клиент не найден");

            _customerRepository.Remove(customer);
            await _customerRepository.SaveAsync();

            return Ok();
        }
    }
}