﻿using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        public string Firstname { get; set; }
        public string Surname { get; set; }
        public string FullName => $"{Firstname} {Surname}";
        public string Email { get; set; }

        public virtual List<CustomerPreference> Preferences { get; set; }
        public virtual List<PromoCode> PromoCodes { get; set; }
    }
}