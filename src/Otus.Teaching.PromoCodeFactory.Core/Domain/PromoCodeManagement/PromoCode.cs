﻿using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class PromoCode
        : BaseEntity
    {
        public Guid PartnerManagerId { get; set; }
        public Guid PreferenceId { get; set; }
        
        public string Code { get; set; }

        public string ServiceInfo { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public string PartnerName { get; set; }

        
        public virtual Employee PartnerManager { get; set; }

        public virtual Preference Preference { get; set; }
    }
}